from intake.catalog import Catalog
from intake.catalog.local import LocalCatalogEntry
import intake_xarray
import intake_esm
import intake
import glob
from pyslk import pyslk
import slkspec
import os
import pandas as pd
import ast

ESM_HEADER={
    "esmcat_version": "0.1.0",
    "attributes": [
        {
            "column_name": "uri",
            "vocabulary": ""
        },
        {
            "column_name": "format",
            "vocabulary": ""
        },
        {
            "column_name": "Var_Name",
            "vocabulary": ""
        },
        {
            "column_name": "Time_Min",
            "vocabulary": ""
        },
    ],
    "assets": {
        "column_name": "uri",
        "format_column_name": "format"
    },
    "aggregation_control": {
        "variable_column_name": "Var_Name",
        "groupby_attrs": [],
        "aggregations": [
            {
                "type": "union",
                "attribute_name": "Var_Name",
                "options": {}
            },
            {
                "type": "join_existing",
                "attribute_name": "Time_Min",
                "options": {
                    "dim": "time",
                    "coords": "minimal",
                    "compat": "override"
                }
            }
        ]
    },
}
class SLKCatalog(Catalog):
    name = 'slk'
    wildcardpath = None
    cache_trunk= next(
        x 
        for x in [
            os.environ.get("SLK_CACHE"),
            f"/scratch/{os.environ.get('USER')[0]}/{os.environ.get('USER')}",
        ]
        if x
    )
    hsm2json = None
                
    def slk_xarray(self,hsm2json):
        for entry in hsm2json:
            path=entry["path"]
            if "netcdf.Data" in entry["tags"]:
                self._entries[path] = LocalCatalogEntry(
                    name=f"data_source_{path}",
                    description=f"data source for {path}",
                    driver=intake_xarray.netcdf.NetCDFSource,  # recursive
                    catalog=self,
                    #args={'urlpath': f"file://{self.cache_trunk}{path}::slk://{path}"},
                    args={'urlpath': f"slk://{path}", "storage_options":{"slk_cache":self.cache_trunk}},
                    metadata=entry["tags"]["netcdf.Data"]
                )
    def _create_filtered_dict(self,hsm2json):
        hsm2json_filtered = list(filter(lambda x: ("netcdf.Data" in x["tags"]), hsm2json))
        for idx,entry in enumerate(hsm2json_filtered):
            for tagname,tag in entry["tags"].items():
                if tagname.startswith("netcdf."):
                    if type(tag) == dict:
                        for k,v in tag.items():
                            hsm2json_filtered[idx].update({k : v})
                    else:
                        hsm2json_filtered[idx].update({tagname.split('.')[1]:tag})  
        return hsm2json_filtered

    def _filter_df(self, df):
        droplist=["protocol","mime_type","location","id","tags","provenance","type","formatVersion","timeStampMillis"]
        to_drop=list(
            filter(
                lambda d: any(
                    pd.isnull(df[d])),
                df.columns
            )
        )
        to_drop+=list(
            filter(
                lambda d: (d in df.columns),
                droplist
            )
        )
        return df.drop(to_drop,axis=1)
        
    def open_esm_datastore(self):
        hsm2json_filtered = self._create_filtered_dict(self.hsm2json)
        df = pd.DataFrame(hsm2json_filtered)
        df = self._filter_df(df)
        df["uri"]=df["path"]
        df["format"]="netcdf"
        df.drop(["path"],axis=1,inplace=True)
        if any(df["Var_Name"].str.contains(',')):
            for name in ["Var_Name", "Var_Std_Name", "Var_Long_Name"]:
                df[name]=df[name].str.split(',')
                df[name]=df[name].astype(str)
                df[name]=df[name].str.replace('[','(').str.replace(']',')')
                df[name]=df[name].apply(ast.literal_eval)

            return intake.open_esm_datastore(
                obj=dict(
                    esmcat=ESM_HEADER,
                    df=df
                ),
                columns_with_iterables=["Var_Name"]
            )
        return intake.open_esm_datastore(
                obj=dict(
                    esmcat=ESM_HEADER,
                    df=df
                )
            )
        
    def __init__(self, wildcardpath, *args, **kwargs):
        self.wildcardpath=wildcardpath
        super().__init__(
            name=f"slk://{wildcardpath}",
            **kwargs
        )

    def _load(self):
        self.hsm2json = eval(
            pyslk.slk_hsm2json(
                self.wildcardpath,
                recursive=True,
                print_hidden=True
            )
        )
        self.slk_xarray(self.hsm2json)


def open_slk(wildcardpath : str, **kwargs):
    return SLKCatalog(wildcardpath, **kwargs)
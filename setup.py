from setuptools import setup, find_packages

setup(
    name="intake_slk",
    version="0.1",
    packages=find_packages(),
    entry_points={
        'intake.drivers': [
            'slk = intake_slk.source:SLKCatalog',
        ],
    },
)
